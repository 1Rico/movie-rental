<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

//start session
session_start();

include 'config/database.php';

if(!isset($_SESSION['name'])){
    header('location: login.php');
    exit();
}

if(!isset($_SESSION['id']) || !isset($_POST['address'])) {

    if (!isset($_SESSION['id'])) {
        try {
            $query = "SELECT id FROM users WHERE email=:email";

            $stmt = $connection->prepare($query);

            $stmt->bindParam(':email', $_SESSION['email']);

            //execute query
            $stmt->execute();

            $num = $stmt->rowCount();

            if ($num > 0) {
                $row = $stmt->fetch();
                $_SESSION['id'] = $row['id'];
                header('location: profile.php');
                exit();
            }

        } catch (PDOException $exception) {
            die('Error: ' . $exception->getMessage());
        }
    } }else {
    try{

        //set variables
        $address = $_POST['address'];
        $id = $_SESSION['id'];
        $created_at = date('Y-m-d H:i:s');

//        var_dump($_SESSION); exit();
        $query =  "SELECT * FROM profiles WHERE user_id=:id";

        //prepare query
        $stmt = $connection->prepare($query);

        //bind parameters
        $stmt->bindParam(':id', $id);

        //execute query
        $stmt->execute();

        $num = $stmt->rowCount();

        if($num == 0){
            $query = "INSERT INTO profiles SET user_id=:id, address=:address, created_at=:created_at";

            $stmt = $connection->prepare($query);

            $stmt->bindParam(':id', $id );
            $stmt->bindParam(':address', $address );
            $stmt->bindParam(':created_at', $created_at);

            if ($stmt->execute()){
                //success
                header("location: profile.php?success=".urlencode("Profile Created Successfully"));
                exit();
            } else {
                return "<div class='alert alert-danger'>Unable to save record</div>";
            }
        } else {

            $query = "UPDATE profiles SET address=:address WHERE user_id=:id";

            $stmt = $connection->prepare($query);

            $stmt->bindParam(':address', $address);
            $stmt->bindParam(':id', $id);

            if($stmt->execute()){
                header("location: profile.php?success=".urlencode("Profile Updated Successfully"));
                exit();
            } else {
                return "<div class='alert alert-danger'>Unable to save record</div>";
            }
        }

    } catch (PDOException $exception) {
        die('Error: '.$exception->getMessage());
    }

}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>LoveFilm - Dashboard</title>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
</head>

<body>
<?php include "inc/header.php"; ?>
<section>
    <div class="banner_big page_head">

    </div>

    <div style="padding: 6em 0; background: #f7f7f7;">

        <h2 class="title">
            Welcome <? $_SESSION['name'] ?>
        </h2>




        <div class="container">
            <?php

            if(isset($_GET['success'])) {
                $success = $_GET['success']; ?>
                <div class='alert alert-success'><?php echo $success ?></div>
            <?php } ?>

            <form action="profile.php" method="post">
                <div class="row">
                    <div class=" form-group col-md-6">
                        <input type="text" class="form-control" name="name" value="<?php echo  $_SESSION['name'] ?>" id="name">
                    </div>
                    <div class=" form-group col-md-6 ">
                        <input type="text" class="form-control" readonly value="<?php echo $_SESSION['email'] ?>" name="email" id="email">
                    </div>
                </div>
                <div class="form-group">
                    <textarea name="address" class="form-control" id="address" placeholder="Enter Address"></textarea>
                </div>
                <div class="form-group text-center">
                    <input type="submit" class="btn btn-lg btn-success" value="Send">
                </div>
            </form>
        </div>
    </div>


    <div class="contact-footer" id="footer">
        <div class="container">
            <h2 class="title w3" style="color: white">Get In Touch
                <!--                <br>-->
                <!--                <small>-->
                <!--                    <a class="btn btn-info" href="#" id="address_toggle">Toggle Adress</a>-->
                <!--                </small>-->
            </h2>

            <form action="contact.php" id="submitForm" method="post">
                <div class="row">
                    <div class="col-md-6">
                        <input type="text" required name="name" id="name" placeholder="Enter Name">
                        <p class="text-danger italic" id="name_error"></p>
                    </div>
                    <div class="col-md-6 ">
                        <input type="email" required name="email" id="email" placeholder="Enter Email">>
                        <p class="text-danger italic" id="email_error"></p>
                    </div>
                </div>
                <!--                <div class="row" id="address_div">-->
                <!--                    <div class="col-md-6">-->
                <!--                        <input type="text" name="address" id="address" placeholder="Enter Address">-->
                <!--                        <!-- <p style="color: white;"></p> -->-->
                <!--                        <p class="text-danger italic" id="address_error"></p>-->
                <!--                    </div>-->
                <!--                    <div class="col-md-6 ">-->
                <!--                        <input type="text" name="phone" id="phone" placeholder="Enter Phone">>-->
                <!--                        <p class="text-danger italic" id="phone_error"></p>-->
                <!--                    </div>-->
                <!--                </div>-->
                <textarea name="message" required id="message" placeholder="Enter Message"></textarea>
                <p class="text-danger italic" id="message_error"></p>
                <div class="con-form text-center">
                    <input type="submit" value="Send">
                </div>
            </form>
            <p class="contact-info">&copy; 2017 LoveFilm . All rights reserved
            </p>
        </div>
    </div>

</section>
<script src="js/jquery-3.3.1.js"></script>
<script src="js/bootstrap.js"></script>
<!--<script src="js/validator.js"></script>-->
</body>

</html>