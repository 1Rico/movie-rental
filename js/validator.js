
$(function () {
    //slide in carousel 2 secs after page load
    // setTimeout(() => {
    //     $('#carouselExampleIndicators').slideDown(1000);
    // }, 2000);

    $('#address_toggle').click(function (event) {
        $("#address_div").toggle(1000);
        event.preventDefault();
    });

    $('#submitForm').submit(function () {

        //clear error messages
        $('#name_error').text('');
        $('#email_error').text('');
        $('#message_error').text('');
        $('#address_error').text('');
        $('#phone_error').text('');

        //get input textbox values
        var email = $('#email').val();
        var name = $('#name').val();
        var message = $('#message').val();
        var address = $('#address').val();
        var phone = $('#phone').val();

        //assign 'not provided if address is hidden
        if ($("#address_div").is(":hidden")) {
            address = 'Not Provided';
            phone = 'Not Provided';
        }

        if (email === "") {
            $('#email_error').text('Email is required');
        }
        if (name === "") {
            $('#name_error').text('Name is required');
        }

        if (message === "") {
            $('#message_error').text('Message Content is required');
        }

        if (address === "") {
            $('#address_error').text('Address  is required');
        }
        if (phone === "") {
            $('#phone_error').text('Phone Number is required');
        }


        if (name !== "" && email !== "" && message !== "" && address !== "" && phone !== "") {
            var formData = {
                name: name,
                email: email,
                address: address,
                phone: phone,
                message: message
            }
            console.log(formData);
        }
        return false;

    });

});