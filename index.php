<?php

//start session
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>LoveFilm</title>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
</head>

<body>
<?php include "inc/header.php"; ?>

<section>
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
        </ol>
        <div class="carousel-inner" style="max-height: 730px;">

            <div class="carousel-item active">
                <img class="d-block w-100" src="images/9.jpg" alt="Second slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="images/2.jpg" alt="Second slide">

            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="images/3.jpg" alt="Second slide">

            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="images/4.jpg" alt="Second slide">

            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="images/5.jpg" alt="Third slide">

            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>

    <div style="padding: 6em 0; background: #f7f7f7;">
        <div class="container" id="movies-list">

            <h2 class="title">

                Welcome To LoveFilm
            </h2>

            <p style="line-height: 2.2em; text-align: center">
                We update our movies regularly, weekly and monthly.
                <br>
                <em>Some of our 2018 updated movies include: </em>
            </p>

            <div class="row" id="movies-row1" style="display: none;">
                <div class="col-md-3  movie-item ">
                    <div class="thumbnail">
                        <img src="images/h7.jpg" alt="Lights" style="width:100%">
                        <div class="caption">
                            <p class="movie-name">FAN</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 movie-item ">
                    <div class="thumbnail">

                        <img src="images/c2.jpg" alt="Nature" style="width:100%">
                        <div class="caption">
                            <p class="movie-name">ZOOTOPIA</p>
                        </div>

                    </div>
                </div>
                <div class="col-md-3 movie-item ">
                    <div class="thumbnail">

                        <img src="images/c3.jpg" alt="Fjords" style="width:100%">
                        <div class="caption">
                            <p class="movie-name">DIRTY GRANDPA</p>
                        </div>

                    </div>
                </div>
                <div class="col-md-3 movie-item ">
                    <div class="thumbnail">

                        <img src="images/c4.jpg" alt="Fjords" style="width:100%">
                        <div class="caption">
                            <p class="movie-name">NEIGHBORS 2</p>
                        </div>

                    </div>
                </div>
            </div>

            <div class="row" id="movies-row2" style="display: none;">
                <div class="col-md-3  movie-item ">
                    <div class="thumbnail">

                        <img src="images/c5.jpg" alt="Lights" style="width:100%">
                        <div class="caption">
                            <p class="movie-name">FINDING DORY</p>
                        </div>

                    </div>
                </div>
                <div class="col-md-3 movie-item ">
                    <div class="thumbnail">

                        <img src="images/c6.jpg" alt="Nature" style="width:100%">
                        <div class="caption">
                            <p class="movie-name">SUICIDE SQUAD</p>
                        </div>

                    </div>
                </div>
                <div class="col-md-3 movie-item ">
                    <div class="thumbnail">

                        <img src="images/c7.jpg" alt="Fjords" style="width:100%">
                        <div class="caption">
                            <p class="movie-name">ZOOLANDER</p>
                        </div>

                    </div>
                </div>
                <div class="col-md-3 movie-item ">
                    <div class="thumbnail">

                        <img src="images/c8.jpg" alt="Fjords" style="width:100%">
                        <div class="caption">
                            <p class="movie-name">RIDE ALONG 2</p>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>



    <div class="services">
        <div class="container">

            <div class="offer agile">
                <h3>
                    Our Services
                </h3>
                <h4>We are the best film rental company in the country.</h4>

            </div>
        </div>
    </div>




    <div class="features">
        <div class="container">
            <h2 class="title">
                Features
            </h2>
            <p class="movie-name">
                We are readily availabe to attend to your any complaints.
                <br>With about 300 service agents, we treat your complaints as important.
                <br> +2348060189124
                <br> emroking@yahoo.com
            </p>
        </div>
    </div>

    <div class="contact-footer" id="footer">
        <div class="container">
            <h2 class="title w3" style="color: white">Get In Touch
<!--                <br>-->
<!--                <small>-->
<!--                    <a class="btn btn-info" href="#" id="address_toggle">Toggle Adress</a>-->
<!--                </small>-->
            </h2>

            <form action="contact.php" id="submitForm" method="post">
                <div class="row">
                    <div class="col-md-6">
                        <input type="text" required name="name" id="name" placeholder="Enter Name">
                        <p class="text-danger italic" id="name_error"></p>
                    </div>
                    <div class="col-md-6 ">
                        <input type="email" required name="email" id="email" placeholder="Enter Email">>
                        <p class="text-danger italic" id="email_error"></p>
                    </div>
                </div>
                <!--                    <div class="row" id="address_div">-->
                <!--                        <div class="col-md-6">-->
                <!--                            <input type="text" name="address" id="address" placeholder="Enter Address">-->
                <!--                            <!-- <p style="color: white;"></p> -->-->
                <!--                            <p class="text-danger italic" id="address_error"></p>-->
                <!--                        </div>-->
                <!--                        <div class="col-md-6 ">-->
                <!--                            <input type="text" name="phone" id="phone" placeholder="Enter Phone">>-->
                <!--                            <p class="text-danger italic" id="phone_error"></p>-->
                <!--                        </div>-->
                <!--                    </div>-->
                <textarea name="message" required id="message" placeholder="Enter Message"></textarea>
                <p class="text-danger italic" id="message_error"></p>
                <div class="con-form text-center">
                    <input type="submit" value="Send">
                </div>
            </form>
            <p class="contact-info">&copy; 2017 LoveFilm . All rights reserved
            </p>
        </div>
    </div>

</section>
<script src="js/jquery-3.3.1.js"></script>
<script src="js/bootstrap.js"></script>
<!--    <script src="js/validator.js"></script>-->
</body>

</html>