<header class=" header ">
    <div>

        <nav class="navbar navbar-expand-lg navbar-dark dark-nav">
            <a class="navbar-brand" href="#">
                <img src="images/love-logo.png" width="100" height="70" class="d-inline-block align-top" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="index.php">Home

                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="gallery.php">Gallery
                        </a>

                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="contact.php">Contact Us
                            <span class="sr-only">(current)</span></a>
                    </li>

                    <?php if(isset($_SESSION['name'])): ?>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Hi, <?php echo $_SESSION['name']; ?>!</a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="profile.php">Profile</a>
                                <a class="dropdown-item" href="videos/index.php">Videos</a>

                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="logout.php">Logout</a>
                            </div>
                        </li>

                    <?php else: ?>
                        <li class="nav-item">
                            <a class="nav-link" href="register.php">Register</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="login.php">Login</a>
                        </li>
                    <?php endif; ?>

                </ul>

            </div>
        </nav>
    </div>

</header>