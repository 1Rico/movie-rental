<?php session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>LoveFilm</title>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
<?php include "inc/header.php"; ?>
<section>
    <div class="banner_big page_head">

    </div>

    <div style="padding: 6em 0; background: #f7f7f7;">
        <h2 class="title">
            Our Movies.
        </h2>
        <div class="container">
            <div class="text-center" id="loader">
                <img src="images/processing.gif">
            </div>

            <div class="row" id="videos_div"></div>
<!--content-->
        </div>
    </div>

    <div class="contact-footer" id="footer">

        <div class="container">
            <h2 class="title w3" style="color: white">Get In Touch
                <!--            <br>-->
                <!--            <small>-->
                <!--                <a class="btn btn-info" href="#" id="address_toggle">Toggle Adress</a>-->
                <!--            </small>-->
            </h2>

            <form action="contact.php" id="submitForm" method="post">
                <div class="row">
                    <div class="col-md-6">
                        <input type="text" required name="name" id="name" placeholder="Enter Name">
                        <!-- <p style="color: white;">John Deo</p> -->
                        <p class="text-danger italic" id="name_error"></p>
                    </div>
                    <div class="col-md-6 ">
                        <input type="email" required name="email" id="email" placeholder="Enter Email">>
                        <p class="text-danger italic" id="email_error"></p>
                    </div>
                </div>
                <textarea name="message" required id="message" placeholder="Enter Message"></textarea>
                <p class="text-danger italic" id="message_error"></p>
                <div class="con-form text-center">
                    <input type="submit" value="Send">
                </div>
            </form>
            <p class="contact-info">&copy; 2017 LoveFilm . All rights reserved
            </p>
        </div>
    </div>

    <div id="myMhodal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Rate Movie</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>

    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title">Modal Header</h6>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                </div>
                <div class="modal-body">
                    <p class="text-info">Rate movie</p>
                    <form method="post" action="api/Controllers/rateMovie.php">
                        <input class="form-control" type="number" min="0" required name="rating"><small class="text-warning">(out of 5)</small>
                        <input type="hidden" name="video_id" id="video_id" value=0>
                        <br />
                        <button type="submit" class="btn btn-outline-success btn-sm">Submit</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
</section>
<script src="js/jquery-3.3.1.js"></script>
<script src="js/bootstrap.js"></script>
<!--<script src="js/validator.js"></script>-->
<script type="text/javascript">
    $('#myModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) ;// Button that triggered the modal
        var id = button.data('id'); // Extract info from data-* attributes
        $("#video_id").val(id);
    });

    $(document).ready(function(){

        fetch_data();

        function fetch_data()
        {
            $.ajax({
                url:"api/Controllers/videos.php",
                success:function(data)
                {
                    $("#videos_div").html(data);
                    $("#loader").hide();
                }
            })
        }});
</script>
</body>

</html>