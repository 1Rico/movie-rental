<?php
/*
A year is said to be a leap year if:
    1. year % 4 == 0 : It is evenly divisible by 4
    2. AND % 100 != 0 : AND Not evenly divisible by 100
    3. OR year % 400 == 0 : OR Evenly divisible by 400
*/

// get start year
$timestamp = strtotime(1980);
$year = date('Y', $timestamp);

//get end year
$timestamp = strtotime(2018);
$stopYear = date('Y', $timestamp);

//counter
$counter = 0;

//loop through years form 1980 till date
while ($year <= $stopYear) {

    if(isLeapYear($year)){
        $counter ++; //increment leap year count by 1
        echo $year.' Leap Year'.'<br>';
    } else {
        echo $year.'<br>';
    }
    $year++;
}

echo '<br>'. 'Total Number of Leap Years is: '.$counter;

// Returns true if year is leap year else false
function isLeapYear($year){
    $isLeapYear = false;
    if(($year % 4) == 0 && ($year % 100) != 0 || ($year % 400) == 0) {
        $isLeapYear = true;
    }
    return $isLeapYear;
}