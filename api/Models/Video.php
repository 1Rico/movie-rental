<?php
/**
 * Created by PhpStorm.
 * User: rico
 * Date: 01/09/2018
 * Time: 15:37
 */
session_start();
Class Video{
    private $connection;
    private $table = "videos";

    public $id;
    public $title;
    public $genre;
    public $pic_url;
    public $created_at;
    public $rating;
    public $user_id;

    public function __construct($db)
    {
        $this->connection = $db;
    }

    function getVideos(){
        $query = "SELECT * FROM ".$this->table." ORDER BY created_at";

        $stmt = $this->connection->prepare($query);

        $stmt->execute();

        return $stmt;
    }

    function rateMovie(){
        try{
            $query = "UPDATE ". $this->table . " SET rating = :rating WHERE id = :id";
            $stmt = $this->connection->prepare($query);

            //bind parameters
            $stmt->bindParam(':id',$this->id);
            $stmt->bindParam(':rating',$this->rating);


            if($stmt->execute()){

                return true;
            }
            return false;
        }
            //show error
        catch (PDOException $exception) {
            die('ERROR: '.$exception->getMessage());
        }

    }

    function create(){
        $target_dir = "../uploads";
        $target_file = $target_dir.basename($_FILES['cover_image']['name']);

        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

//    validate image type
        if(isset($_POST['submit'])) {
            $check = getimagesize($_FILES['cover_image']['tmp_name']);

            if($check !== false) {
                echo "File is image - ".$check["mime"]. ".";
                $uploadOk = 1;
            } else {
                echo "Not an image.";
                $uploadOk = 0;
            }
        }

        echo $uploadOk . " ". basename($_FILES['cover_image']['name']) . '<br>';


        //file exists?
        if(file_exists($target_file)) {
            echo "File exits!";
            $uploadOk = 0;
        }


        //check status of upload

        if($uploadOk = 0){
            echo "File not uploaded!";
        } else {
            //try upload
            if(move_uploaded_file($_FILES['cover_image']['tmp_name'], $target_file)){
                echo "File ".basename($_FILES['cover_image']['name']). "has been uploaded";
            } else {
                "There was an error uploading the file";
            }

        } try{
            $query = "INSERT INTO videos SET user_id=:id, title=:title, genre=:genre, pic_url=:pic_url, created_at=:created_at";
            $stmt = $this->connection->prepare($query);

            //get form data
            $title = $_POST['title'];
            $genre = $_POST['genre'];
            $pic_url = basename($_FILES['cover_image']['name']);
            $id = $_SESSION['id'];
            $created_at = date('Y-m-d H:i:s');

            //bind params
            $stmt->bindParam(':id', $id);
            $stmt->bindParam(':title', $title);
            $stmt->bindParam(':genre', $genre);
            $stmt->bindParam(':created_at', $created_at);
            $stmt->bindParam(':pic_url', $pic_url);

            if($stmt->execute()){

                header("location: index.php?success=".urlencode("Movie Uploaded Successfully"));
                exit();
            } else {
                echo "<div class='alert alert-danger'>Unable to upload movie!</div>";
            }



        } catch (PDOException $exception) {
            die("Error: ".$exception->getMessage());
        }

        // sanitize
        $this->title=htmlspecialchars(strip_tags($this->title));
        $this->genre=htmlspecialchars(strip_tags($this->genre));
        $this->user_id=htmlspecialchars(strip_tags($this->user_id));
        $this->created_at=htmlspecialchars(strip_tags($this->created_at));}

}