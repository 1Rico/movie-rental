<?php
/**
 * Created by PhpStorm.
 * User: rico
 * Date: 01/09/2018
 * Time: 22:01
 */
session_start();
class User{
    private $connection;
    private $table = "users";

    public $name;
    public $email;
    public $password;
    public $created_at;

    public function __construct($db)
    {
        $this->connection = $db;
    }

    function create()
    {
        try {
            $query = 'INSERT INTO users SET name=:name, email=:email, password=:password, created_at=:created_at';

            //prepare query
            $stmt = $this->connection->prepare($query);
            //post values
            $this->password = hash('sha256', $_POST['password']);
            $date = date('Y-m-d H:i:s');

            //bind parameters
            $stmt->bindParam(':name', $this->name);
            $stmt->bindParam(':email', $this->email);
            $stmt->bindParam(':password', $this->password);
            $stmt->bindParam(':created_at', $date);

            if ($stmt->execute()) {
                $_SESSION['name'] = $this->name;
                $_SESSION['email'] = $this->email;
                return true;
            }
            return false;
        }
            //show error
        catch (PDOException $exception) {
            die('ERROR: '.$exception->getMessage());
        }}

    function login()
    {
        try {
            $query = "SELECT * FROM " . $this->table . " WHERE email=:email AND password=:password";
            $stmt = $this->connection->prepare($query);

            //bind parameters
//            $email = $_POST['email'];
            $this->password = hash('sha256', $_POST['password']);

            $stmt->bindParam(':email', $this->email);
            $stmt->bindParam(':password', $this->password);


            if ($stmt->execute()) {
                $num = $stmt->rowCount();

                if ($num > 0) {
                    $row = $stmt->fetch();

                    $_SESSION['name'] = $row['name'];
                    $_SESSION['email'] = $row['email'];
                    $_SESSION['id'] = $row['id'];
                    return true;
                }
                return false;
            }
            return false;
        } //show error
        catch (PDOException $exception) {
            die('ERROR: ' . $exception->getMessage());
        }

    }
}