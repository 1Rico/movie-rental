<?php

//include db
include_once "../../config/database.php";
include_once "../Models/Video.php";

//instantiate db
$database = new Database();
$db = $database->getConnection();

$video = new Video($db);

// get posted data
$rating = $_POST['rating'];
$id = $_POST['video_id'];

// set product property values
$video->rating = $rating;
$video->id = $id;

if($video->rateMovie()){
    echo '{';
    echo '"message": "Rating saved successfully."';
    echo '}<br />';
    echo '<a href="javascript:history.go(-1)">Back</a>';
}
// if unable to create the product, tell the user
else{
    echo '{';
    echo '"message": "Unable to save rating."';
    echo '}<br />';
    echo '<a href="javascript:history.go(-1)">Back</a>';
}
