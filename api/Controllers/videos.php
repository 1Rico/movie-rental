<?php
/**
 * Created by PhpStorm.
 * User: rico
 * Date: 08/09/2018
 * Time: 09:24
 */


//headers
//header("Access-Control-Allow-Origin: *");
//header("Content-Type: application/json; charset=UTF-8");

//include db
include_once "../../config/database.php";
include_once "../Models/Video.php";

//instantiate db
$database = new Database();
$db = $database->getConnection();

$video = new Video($db);

$stmt = $video->getVideos();
$num = $stmt->rowCount();


function getRating($n){
    $rating = '';
    for($i = 0; $i < 5; $i++){
        if($i < $n){
             $rating .= "<span class='fa fa-star checked'></span>";
        } else {
            $rating.= "<span class='fa fa-star'></span>";
        }
    }
    return $rating;
}

function showRating($video_id){
    $button = '';

    if(isset($_SESSION['name'])){
        $button = "<button data-id=".$video_id." class='btn btn-outline-success btn-sm' data-toggle='modal' data-target='#myModal'>Rate</button>";
    } else {
        $button = "<p class='text-danger'>Please Login To Rate</p>";
    }
    return $button;

}
// check if more than 0 record found
if($num>0){

    // products array
    $videos_arr=array();
    $videos_arr["videos"] = array();

    // retrieve our table contents
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        extract($row);

        $video_item=array(
            "id" => $id,
            "user_id" => $user_id,
            "title" => html_entity_decode($title),
            "genre" => html_entity_decode($genre),
            "pic_url" => $pic_url,
            "rating" => $rating,
            "created_at" => $created_at,
        );

        array_push($videos_arr["videos"], $video_item);
    }

    $result = $videos_arr;
    $row = "";
    $i;

    foreach($result['videos'] as $video)
    {
        $row .= "
            <div class='col-md-3  movie-item '>
                <div class='thumbnail'>
                <p class='movie-name'>".$video['title']."</p>
                    <img src=".$video['pic_url']." alt='Lights' style='width:100%'>
                    
                    <div class='caption'>
                        ".getRating($video['rating'])."
                    </div>
                    <div class='caption pull-right'>
                       ".showRating($video['id'])."
                       </div>
                </div>
                
            </div>";
    }
    echo $row;

}

else{
    echo json_encode(
        array("message" => "No videos found.")
    );
}