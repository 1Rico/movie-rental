<?php

//include db
include_once "../../config/database.php";
include_once "../Models/User.php";

//instantiate db
$database = new Database();
$db = $database->getConnection();

$user = new User($db);

// get posted data
$email = $_POST['email'];
$name = $_POST['name'];
$password = $_POST['password'];

// set product property values
$user->email = $email;
$user->name = $name;
$user->password = $password;


if($user->create()) {
    header('location: ../../gallery.php');
    exit();
}
// if unable to create the product, tell the user
else{
    echo '{';
    echo '"message": "Unable to create user."';
    echo '}';
}
