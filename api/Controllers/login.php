<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

//include db
include_once "../../config/database.php";
include_once "../Models/User.php";

//instantiate db
$database = new Database();
$db = $database->getConnection();

$user = new User($db);

// get posted data
$email = $_POST['email'];
$password = $_POST['password'];

// set product property values
$user->email = $email;
$user->password = $password;

if($user->login()) {
    header('location: ../../index.php');
    exit();
}
// if unable to create the product, tell the user
else{
    echo '{';
    echo '"message": "Unable to create product."';
    echo '}';
}
