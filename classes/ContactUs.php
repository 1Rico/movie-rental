<?php

use PHPMailer\PHPMailer\PHPMailer;
require 'vendor/autoload.php';

class ContactUs{
    public static function process($param){
        $name = $param['name'];
        $email = $param['email'];
        $message = $param['message'];

        date_default_timezone_set('Africa/Lagos');

        //csv file(create or open)
        $file = fopen('contact.csv', 'a');

        //csv file column(first time)
        fputcsv($file, array('Name', 'Email', 'Message', 'Date'));

        //insert details from contact form
        fputcsv($file, array($name, $email, $message, date('Y-m-d h:i:sa')));

        //close file
        fclose($file);

        //email content
        $body = "Hello ".$name.",\n\nThank you for contacting us.\n\nWe will be in touch soon.\n\nRegards.";

        //send mail
        // mail($param['email'], "LoveFilm Contact", $message, $headers);
        // $mail = new PHPMailer;

        $mail = new PHPMailer;
        $mail->isSMTP();
        $mail->Host = 'smtp.gmail.com';
        $mail->Port = 465;
        $mail->SMTPDebug = 0;
        $mail->SMTPAuth = true;
        $mail->SMTPSecure = 'ssl';

        //Username to use for SMTP authentication
        $mail->Username = 'booustsmtp@gmail.com';

        //Password to use for SMTP authentication
        $mail->Password = '1a2b3c4d5eBOOUST';
        $mail->isHTML();

        $mail->setFrom('admin@lovefilm.com', 'LoveFilm Admin');
        $mail->addAddress($email);
        $mail->Subject = 'Contact form';
        $mail->Body = $body;


        if (!$mail->send()) {
            $msg = "Mailer Error: " . $mail->ErrorInfo;
        } else {
            $msg = "Message sent!";
        }
        return $msg;
    }
}