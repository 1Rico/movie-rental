<?php

//start session
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>LoveFilm - Contact Us</title>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
</head>

<body>
<?php include "inc/header.php"; ?>
<section>
    <div class="banner_big page_head">

    </div>

    <div style="padding: 6em 0; background: #f7f7f7;">
        <h2 class="title">
            Contact Us.
        </h2>

        <?php

        if (isset($_POST['name']) && isset($_POST['email']) && isset($_POST['message']) ):
            include "classes/ContactUs.php";

            ContactUs::process($_POST);

           ?>
        <div class="container text-center">
            <h1>Thank you for contacting us!</h1>
            <p>You will receive an email shortly.</p>

            <br>
            <div class="form-group">
                <a href="contact.php" class="btn btn-lg btn-success">Contact Us</a>
            </div>
        </div>

        <?php else: ?>

        <div class="container">
            <form action="contact.php" method="post">
                <div class="row">
                    <div class=" form-group col-md-6">
                        <input type="text" class="form-control" name="name" id="name" placeholder="Enter Name">
                    </div>
                    <div class=" form-group col-md-6 ">
                        <input type="text" class="form-control" name="email" id="email" placeholder="Enter Email">
                    </div>
                </div>
                <div class="form-group">
                    <textarea name="message" class="form-control" id="message" placeholder="Enter Message"></textarea>
                </div>
                <div class="form-group text-center">
                    <input type="submit" class="btn btn-lg btn-success" value="Send">
                </div>
            </form>
        </div>
    </div>
    <?php endif; ?>

    <div class="contact-footer" id="footer">
        <div class="container">
            <h2 class="title w3" style="color: white">Get In Touch
<!--                <br>-->
<!--                <small>-->
<!--                    <a class="btn btn-info" href="#" id="address_toggle">Toggle Adress</a>-->
<!--                </small>-->
            </h2>

            <form action="contact.php" id="submitForm" method="post">
                <div class="row">
                    <div class="col-md-6">
                        <input type="text" required name="name" id="name" placeholder="Enter Name">
                        <p class="text-danger italic" id="name_error"></p>
                    </div>
                    <div class="col-md-6 ">
                        <input type="email" required name="email" id="email" placeholder="Enter Email">>
                        <p class="text-danger italic" id="email_error"></p>
                    </div>
                </div>
<!--                <div class="row" id="address_div">-->
<!--                    <div class="col-md-6">-->
<!--                        <input type="text" name="address" id="address" placeholder="Enter Address">-->
<!--                        <!-- <p style="color: white;"></p> -->-->
<!--                        <p class="text-danger italic" id="address_error"></p>-->
<!--                    </div>-->
<!--                    <div class="col-md-6 ">-->
<!--                        <input type="text" name="phone" id="phone" placeholder="Enter Phone">>-->
<!--                        <p class="text-danger italic" id="phone_error"></p>-->
<!--                    </div>-->
<!--                </div>-->
                <textarea name="message" required id="message" placeholder="Enter Message"></textarea>
                <p class="text-danger italic" id="message_error"></p>
                <div class="con-form text-center">
                    <input type="submit" value="Send">
                </div>
            </form>
            <p class="contact-info">&copy; 2017 LoveFilm . All rights reserved
            </p>
        </div>
    </div>

</section>
<script src="js/jquery-3.3.1.js"></script>
<script src="js/bootstrap.js"></script>
<!--<script src="js/validator.js"></script>-->
</body>

</html>