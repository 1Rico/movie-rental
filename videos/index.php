<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

//start session
session_start();


if(!isset($_SESSION['name'])){
    header('location: ../index.php');
    exit();
}

require '../config/database.php';

if($_POST){


    $target_dir = "../uploads/";
    $target_file = $target_dir.basename($_FILES['cover_image']['name']);

    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

//    validate image type
    if(isset($_POST['submit'])) {
        $check = getimagesize($_FILES['cover_image']['tmp_name']);

        if($check !== false) {
            echo "FIle is image - ".$check["mime"]. ".";
            $uploadOk = 1;
        } else {
            echo "Not an image.";
            $uploadOk = 0;
        }
    }

    echo $uploadOk . " ". basename($_FILES['cover_image']['name']) . '<br>';


    //file exists?
    if(file_exists($target_file)) {
        echo "File exits!";
        $uploadOk = 0;
    }


    //check status of upload

    if($uploadOk = 0){
        echo "File not uploaded!";
    } else {
        //try upload
        if(move_uploaded_file($_FILES['cover_image']['tmp_name'], $target_file)){
            echo "File ".basename($_FILES['cover_image']['name']). "has been uploaded";
        } else {
            "There was an error uploading the file";
        }

    } try{
        $query = "INSERT INTO videos SET user_id=:id, title=:title, genre=:genre, pic_url=:pic_url, created_at=:created_at";
        $stmt = $connection->prepare($query);

        //get form data
        $title = $_POST['title'];
        $genre = $_POST['genre'];
        $pic_url = basename($_FILES['cover_image']['name']);
        $id = $_SESSION['id'];
        $created_at = date('Y-m-d H:i:s');

        //bind params
        $stmt->bindParam(':id', $id);
        $stmt->bindParam(':title', $title);
        $stmt->bindParam(':genre', $genre);
        $stmt->bindParam(':created_at', $created_at);
        $stmt->bindParam(':pic_url', $pic_url);

        if($stmt->execute()){

            header("location: index.php?success=".urlencode("Movie Uploaded Successfully"));
            exit();
        } else {
            echo "<div class='alert alert-danger'>Unable to upload movie!</div>";
        }



    } catch (PDOException $exception) {
        die("Error: ".$exception->getMessage());
    }
}


$query = "SELECT * FROM videos";

$stmt = $connection->prepare($query);
$stmt->execute();

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>LoveFilm - Dashboard</title>
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
</head>

<body>

<header class=" header ">
    <div>

        <nav class="navbar navbar-expand-lg navbar-dark dark-nav">
            <a class="navbar-brand" href="#">
                <img src="../images/love-logo.png" width="100" height="70" class="d-inline-block align-top" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="../index.php">Home

                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../gallery.php">Gallery
                        </a>

                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="../contact.php">Contact Us
                            <span class="sr-only">(current)</span></a>
                    </li>

                    <?php if(isset($_SESSION['name'])): ?>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Hi, <?php echo $_SESSION['name']; ?>!</a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="../profile.php">Profile</a>
                                <a class="dropdown-item" href="../videos/index.php">Videos</a>

                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="../logout.php">Logout</a>
                            </div>
                        </li>

                    <?php else: ?>
                        <li class="nav-item">
                            <a class="nav-link" href="../register.php">Register</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="../login.php">Login</a>
                        </li>
                    <?php endif; ?>

                </ul>

            </div>
        </nav>
    </div>

</header>
<section>
    <div class="banner_big page_head">
        <h2>Movies</h2>
    </div>

    <div style="padding: 6em 0; background: #f7f7f7;">

        <div class="container">
            <?php

            if(isset($_GET['success'])) {
                $success = $_GET['success']; ?>
                <div class='alert alert-success'><?php echo $success ?></div>
            <?php } ?>
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Title</th>
                        <th>Genre</th>
                        <th>Date Uploaded</th>
                        <td></td>
                        <td></td>
                        <td></td>

                    </tr>
                    </thead>
                    <tbody>
                    <?php while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){ ?>
                        <tr>
                            <td></td>
                            <td><?php echo htmlspecialchars($row['title'], ENT_HTML5, 'UTF-8')?></td>
                            <td><?php echo htmlspecialchars($row['genre'], ENT_HTML5, 'UTF-8')?></td>
                            <td><?php echo $row['title'];?></td>
                            <th><a href="#">View</a> </th>
                            <th><a href="#">Edit</a> </th>
                            <th><a href="#">Delete</a> </th>

                        </tr>
                    <?php } ?>
                    </tbody>
                </table>

            </div>
            <div class="text-center">
                <button class="btn btn-outline-success btn-sm" data-toggle="modal" data-target="#myModal">Add New</button>

            </div>


        </div>
    </div>

    <div class="contact-footer" id="footer">
        <div class="container">
            <h2 class="title w3" style="color: white">Get In Touch</h2>

            <form action="contact.php" id="submitForm" method="post">
                <div class="row">
                    <div class="col-md-6">
                        <input type="text" required name="name" id="name" placeholder="Enter Name">
                        <p class="text-danger italic" id="name_error"></p>
                    </div>
                    <div class="col-md-6 ">
                        <input type="email" required name="email" id="email" placeholder="Enter Email">>
                        <p class="text-danger italic" id="email_error"></p>
                    </div>
                </div>
                <textarea name="message" required id="message" placeholder="Enter Message"></textarea>
                <p class="text-danger italic" id="message_error"></p>
                <div class="con-form text-center">
                    <input type="submit" value="Send">
                </div>
            </form>
            <p class="contact-info">&copy; 2017 LoveFilm . All rights reserved
            </p>
        </div>
    </div>


    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Modal Header</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <form method="post" enctype="multipart/form-data" action="index.php">
                        <div class="form-group">
                            <input class="form-control" type="text" name="title" placeholder="Enter Title" required>
                        </div>
                        <div class="form-group">
                            <input class="form-control" type="text" name="genre" placeholder="Enter Genre" required>
                        </div>

                        <div class="form-group">
                            <input class="form-control" type="file" name="cover_image" required>
                        </div>

                        <input type="submit" class="btn btn-sm btn-success" name="submit">
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-info" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

</section>
<script src="../js/jquery-3.3.1.js"></script>
<script src="../js/bootstrap.js"></script>
<!--<script src="js/validator.js"></script>-->
</body>

</html>